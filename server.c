#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <arpa/inet.h>

#include <rdma/rdma_cma.h> 

#include "common.h"

struct conn_info {
	struct ibv_mr *mr_recv;
	uint32_t *buf_recv;
};

struct rdma_event_channel   *cm_channel;
struct rdma_cm_id           *listen_id; 
struct rdma_cm_id           *cm_id; 
struct rdma_cm_event        *event; 
struct rdma_conn_param      conn_param = { };

struct ibv_pd           *pd; 
struct ibv_comp_channel *comp_chan; 
struct ibv_cq           *cq;  
struct ibv_cq           *evt_cq;
struct ibv_mr           *mr_recv; 
struct ibv_mr           *mr_send; 
struct ibv_qp_init_attr qp_attr = { };
struct ibv_sge          sge; 
struct ibv_send_wr      send_wr = { };
struct ibv_send_wr      *bad_send_wr; 
struct ibv_wc           wc;
void                    *cq_context;

uint32_t                *buf_recv;
uint32_t                *buf_send;

struct conn_info conn[RECV_QUEUE_NUM];

int                     err;
struct ibv_srq *srq;
struct ibv_srq_init_attr srq_init_attr;

struct ibv_recv_wr      recv_wr = { };
struct ibv_recv_wr      *bad_recv_wr = NULL;

void create_srq(struct ibv_pd *pd) {
  
  memset(&srq_init_attr, 0, sizeof(srq_init_attr));
 
  srq_init_attr.attr.max_wr  = 10;
  srq_init_attr.attr.max_sge = 2;

  srq = ibv_create_srq(pd, &srq_init_attr);
  if (!srq) {
    fprintf(stderr, "Error, ibv_create_srq() failed\n");
    return;
  }  
}

void post_send() {

  *buf_send = 20;
  sge.addr                      = (uintptr_t) buf_send;
  sge.length                    = BUFFER_SIZE_INT;
  sge.lkey                      = mr_send->lkey;
  send_wr.opcode                = IBV_WR_SEND;
  send_wr.send_flags            = IBV_SEND_SIGNALED;
  send_wr.sg_list               = &sge;
  send_wr.num_sge               = 1;
  
  if (ibv_post_send(cm_id->qp, &send_wr, &bad_send_wr))
    return;
}

void post_recv_messages() {

  /* Post receive before accepting connection */
	int i = 0;
	for (i = 0; i < RECV_QUEUE_NUM; ++i) {
		printf("start to post recv...%d\n", i);
		sge.addr = (uintptr_t) conn[i].buf_recv; 
		sge.length = BUFFER_SIZE_INT; 
		printf("start to assign recv mr...\n");
		sge.lkey = conn[i].mr_recv->lkey;
		printf("finish to assign recv mr...\n");
		recv_wr.sg_list = &sge; 
		recv_wr.num_sge = 1;
		recv_wr.next = NULL;
		recv_wr.wr_id = i;
		
		if (SUPPORT_SRQ) {
			if (ibv_post_srq_recv(srq, &recv_wr, &bad_recv_wr)) {
				return;
			}
		} else {
			if (ibv_post_recv(cm_id->qp, &recv_wr, &bad_recv_wr)) {
				return;
			}
		}
	}
}

void post_recv_message(int wr_id) {
	sge.addr = (uintptr_t) conn[wr_id].buf_recv; 
	sge.length = BUFFER_SIZE_INT; 
	sge.lkey = conn[wr_id].mr_recv->lkey;

	recv_wr.sg_list = &sge; 
	recv_wr.num_sge = 1;
	recv_wr.next = NULL;
	recv_wr.wr_id = wr_id;
	if (SUPPORT_SRQ) {
		if (ibv_post_srq_recv(srq, &recv_wr, &bad_recv_wr)) {
			return;
		}
	} else {
		if (ibv_post_recv(cm_id->qp, &recv_wr, &bad_recv_wr)) {
			return;
		}
	}
}

int main(int argc, char *argv[]) 
{ 
    struct sockaddr_in      sin;

    cm_channel = rdma_create_event_channel();
    if (!cm_channel) 
        return 1;

    err = rdma_create_id(cm_channel, &listen_id, NULL, RDMA_PS_TCP); 
    if (err) 
        return err;

    sin.sin_family = AF_INET; 
    sin.sin_port = htons(20082);
    sin.sin_addr.s_addr = INADDR_ANY;

    err = rdma_bind_addr(listen_id, (struct sockaddr *) &sin);
    if (err) 
        return 1;
    err = rdma_listen(listen_id, 1);
    if (err)
        return 1;

    err = rdma_get_cm_event(cm_channel, &event);
    if (err)
        return err;

    if (event->event != RDMA_CM_EVENT_CONNECT_REQUEST)
        return 1;

    cm_id = event->id;

    rdma_ack_cm_event(event);

    pd = ibv_alloc_pd(cm_id->verbs);
    if (!pd) 
        return 1;
    
    if (SUPPORT_SRQ) {
      create_srq(pd);
    }

    comp_chan = ibv_create_comp_channel(cm_id->verbs);
    if (!comp_chan)
        return 1;

    cq = ibv_create_cq(cm_id->verbs, 10, NULL, comp_chan, 0); 
    if (!cq)
        return 1;

    if (ibv_req_notify_cq(cq, 0))
        return 1;

		int i = 0;
		for (i = 0; i < RECV_QUEUE_NUM; i++) {
			conn[i].buf_recv = calloc(1, BUFFER_SIZE_INT);
			if (!conn[i].buf_recv)
				return 1;
		}
		
    buf_send = calloc(1, BUFFER_SIZE_INT);
    if (!buf_send) 
      return 1;
		
		for (i = 0; i < RECV_QUEUE_NUM; i++) {
			conn[i].mr_recv = ibv_reg_mr(pd, conn[i].buf_recv, BUFFER_SIZE_INT,
					IBV_ACCESS_LOCAL_WRITE | 
					IBV_ACCESS_REMOTE_READ | 
					IBV_ACCESS_REMOTE_WRITE); 
			if (!conn[i].mr_recv) 
				return 1;
		}

    mr_send = ibv_reg_mr(pd, buf_send, BUFFER_SIZE_INT,
        IBV_ACCESS_LOCAL_WRITE | 
        IBV_ACCESS_REMOTE_READ | 
        IBV_ACCESS_REMOTE_WRITE); 
    if (!mr_send) 
        return 1;

    qp_attr.cap.max_send_wr = 1;
    qp_attr.cap.max_send_sge = 1;
    qp_attr.cap.max_recv_wr = RECV_QUEUE_NUM;
    qp_attr.cap.max_recv_sge = 1;
		//qp_attr.cap.max_inline_data = 0;
		//qp_attr.sq_sig_all = 0;

    if (SUPPORT_SRQ)
      qp_attr.srq = srq;

    qp_attr.send_cq = cq;
    qp_attr.recv_cq = cq;

    qp_attr.qp_type = IBV_QPT_RC;
    
    err = rdma_create_qp(cm_id, pd, &qp_attr); 
    if (err) 
        return err;
		printf("preparing to post recv...\n");
    post_recv_messages();
    conn_param.srq = 1;

    err = rdma_accept(cm_id, &conn_param); 
    if (err) 
        return 1;
    
    err = rdma_get_cm_event(cm_channel, &event);
    if (err) 
        return err;

    if (event->event != RDMA_CM_EVENT_ESTABLISHED)
        return 1;

    rdma_ack_cm_event(event);
    
    uint64_t need_stop = 0;
    while (1) {
      need_stop++; 
      if (ibv_get_cq_event(comp_chan, &evt_cq, &cq_context)) {
        return 1;
			}
      int ret = 0;
      do {
        ret = ibv_poll_cq(cq, 1, &wc);
      } while (ret == 0);
      
      if (wc.status != IBV_WC_SUCCESS) {
        return 1;
			}

			if (ibv_req_notify_cq(cq, 0)) {
        return 1;
			}

      ibv_ack_cq_events(cq, 1);

      post_recv_message(wc.wr_id);
      post_send();
		
      if (ibv_get_cq_event(comp_chan, &evt_cq, &cq_context)) {
				return 1;
			}
      
      do {
        ret = ibv_poll_cq(cq, 1, &wc);   
      } while (ret == 0);

      if (wc.status != IBV_WC_SUCCESS) {
        return 1;
			}

      if (ibv_req_notify_cq(cq, 0)) {
        return 1;
			}

      ibv_ack_cq_events(cq, 1);
      if (need_stop >= STOP) break;
			printf("new round...%d\n", need_stop);
    }
    return 0;
}
